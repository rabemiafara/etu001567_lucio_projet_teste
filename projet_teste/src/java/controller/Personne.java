/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.*;
import java.util.ArrayList;
import java.util.Date;




@Controller
@Lien(lien = "personne")
public class Personne {
    
    private String nom;
    private Date naissance;
    
    //  /personne/saluet.do
    @Lien(lien = "saluet")
    public ModelView teste(){       
        ArrayList liste = new ArrayList();
        liste.add(this.nom);
        liste.add(this.naissance);
        return new ModelView("teste", liste);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getNaissance() {
        return naissance;
    }

    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }
    
}
